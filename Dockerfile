ARG BASE_CONTAINER=jupyter/datascience-notebook
ARG BASE_TAG=latest
FROM $BASE_CONTAINER:$BASE_TAG

LABEL maintainer="Unai Irigoyen <u.irigoyen@gmail.com>"

USER root

# R pre-requisites
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    p7zip-full \
    unrar \
    git && \
    rm -rf /var/lib/apt/lists/*

USER $NB_UID

RUN python -m pip install \
    patool \
    pyunpack

RUN pip install --upgrade jupyterlab-git && \
    jupyter lab build

RUN pip install dask-kubernetes --upgrade
